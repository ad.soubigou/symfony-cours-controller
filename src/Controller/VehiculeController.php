<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Attribute\Route;

class VehiculeController extends AbstractController
{
    // #[Route('/vehicule', name: 'app_vehicule')]
    // public function index(): Response
    // {
    //     // forward() va rediriger vers l'action index de HomeController (même URL vehicule/)
    //     return $this->forward('App\Controller\HomeController::index', ['nom' => 'Doe',]);
    // }

    // #[Route('/vehicule', name: 'app_vehicule')]
    // public function index(): Response
    // {
    //     // on génère une URL (home/Santana) puis on redirige
    //     $url = $this->generateUrl('home_route', array('nom' => 'Santana',));
    //     return new RedirectResponse($url);
    //     // On peut aussi utiliser la méthode redirect() de AbstractController
    //     // return $this->redirect($url);
    // }

    // Même chose avec la méthode redirectToRoute
    // #[Route('/vehicule', name: 'app_vehicule')]
    // public function index(): Response
    // {
    //     return $this->redirectToRoute('home_route', array('nom' => 'Santana',));
    // }

    // La méthode redirect() permet aussi de rediriger vers une URL externe
    #[Route('/vehicule', name: 'app_vehicule')]
    public function index(): Response
    {
        return $this->redirect('http://symfony.com/doc');
    }
}
