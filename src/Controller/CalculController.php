<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class CalculController extends AbstractController
{
    #[Route('/calcul/{op}', name: 'app_calcul')]
    public function apply(Request $request): Response
    {
        $nom = $request->get('op');
        $params = $request->query->all();
        if (is_numeric($params['var1']) && is_numeric(($params['var2']))) {
            switch ($nom) {
                case "plus":
                    $resultat = $params['var1'] . " + " . $params['var1'] . " = " . $params['var1'] + $params['var2'];
                    break;
                case "moins":
                    $resultat = $params['var1'] . " - " . $params['var1'] . " = " . $params['var1'] - $params['var2'];
                    break;
                case "fois":
                    $resultat = $params['var1'] . " * " . $params['var1'] . " = " . $params['var1'] * $params['var2'];
                    break;
                case "div":
                    if ($params['var2'] == '0') {
                        $resultat = "Infinity";
                    } else {
                        $resultat = $params['var1'] . " / " . $params['var1'] . " = " . $params['var1'] / $params['var2'];
                    }
                    break;
                default:
                    $resultat = "erreur : opérateur non défini";
            }
        } else {
            $resultat = "erreur : variables non numériques";
        }
        return $this->render('calcul/index.html.twig', ['controller_name' => $resultat]);
    }
}
