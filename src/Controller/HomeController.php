<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Attribute\Route;
use App\Entity\Personne;

// #[Route('/home')]
class HomeController extends AbstractController
{
    // #[Route('/')]
    // #[Route('/home/', name: 'app_home')]
    // // #[Route('/index')]
    // public function index(): Response
    // {
    //     return $this->render('home/index.html.twig', [
    //         'controller_name' => 'HomeController',
    //     ]);
    // }

    // #[Route('/home/{nom}', name: 'app_home')]
    // public function index(string $nom): Response
    // {
    //     return $this->render('home/index.html.twig', [
    //         'controller_name' => $nom,
    //     ]);
    // }

    // On définit une contraite au paramètre en utilisant une regex
    // #[Route('/home/{age}', name: 'app_home', requirements: ["age" => "\d{2,3}"])]
    // public function index(string $age): Response
    // {
    //     return $this->render('home/index.html.twig', [
    //         'controller_name' => $age,
    //     ]);
    // }

    // La contrainte peut être accollée au paramètre
    // #[Route('/home/{age<\d+>}', name: 'app_home')]
    // public function index(string $age): Response
    // {
    //     return $this->render('home/index.html.twig', [
    //         'controller_name' => $age,
    //     ]);
    // }

    // Rendre le paramètre optionnel en lui attribuant une valeur par défaut
    // #[Route('/home/{age<\d+>}', name: 'app_home')]
    // public function index(int $age = 7): Response
    // {
    //     return $this->render('home/index.html.twig', [
    //         'controller_name' => $age,
    //     ]);
    // }

    // Pour accepter la valeur null (retirer le typage de l'attribut)
    // #[Route('/home/{age?}', name: 'app_home')]
    // public function index(?int $age): Response
    // {
    //     return $this->render('home/index.html.twig', [
    //         'controller_name' => $age,
    //     ]);
    // }

    // On définit des constantes qu'on récupère comme des paramètres de substitution mais ne font pas partie de la route
    // #[Route('/home/{age}', name: 'home_route', defaults: ["nom" => "doe", "prenom" => "john"])]
    // public function index(int $age, string $nom, string $prenom): Response
    // {
    //     return $this->render('home/index.html.twig', [
    //         'controller_name' => "$age $nom $prenom",
    //     ]);
    // }

    // // Priorité (l'action index2() ne s'affiche pas car la route demandée home/index matche avec la route index())
    // #[Route('/home/{nom}', name: 'home_route')]
    // public function index(string $nom): Response
    // {
    //     return $this->render('home/index.html.twig', ['controller_name' => $nom,]);
    // }

    // // On ajoute "priority: 2" pour donner la priorité à index2(). Prioriété par défaut = 0, prioriété à la plus grande valeur prend le pas.
    // // #[Route('/home/index', name: 'home_route2')]
    // #[Route('/home/index', name: 'home_route2', priority: 2)]
    // public function index2(): Response
    // {
    //     return $this->render('home/index.html.twig', ['controller_name' => 'HomeController',]);
    //     }

    // // On récupère un paramètre de substitution en utilisant l'objet request
    // #[Route('/home/{nom}', name: 'home_route')]
    // public function index(Request $request): Response
    // {
    //     $nom = $request->get('nom');
    //     return $this->render('home/index.html.twig', ['controller_name' => $nom,]);
    // }

    // On peut utiliser le raccourci attributes pour récupérer tous les paramètres
    // #[Route('/home/{nom}/{prenom}', name: 'home_route')]
    // public function index(Request $request): Response
    // {
    //     $params = $request->get('_route_params');
    //     return $this->render('home/index.html.twig', ['controller_name' => implode(" ", $params),]);
    // }

    // Pour les paramètres hors routes, pas besoin de les déclarer dans la route
    // #[Route('/home', name: 'home_route')]
    // public function index(Request $request): Response
    // {
    //     $nom = $request->query->get('nom');
    //     return $this->render('home/index.html.twig', ['controller_name' => $nom,]);
    // }

    // Pour récupérer tous les paramètres hors routes
    // #[Route('/home', name: 'home_route')]
    // public function index(Request $request): Response
    // {
    //     $params = $request->query->all();
    //     return $this->render('home/index.html.twig', ['controller_name' => implode("", $params),]);
    // }

    // Pour récupérer le nom d'une route en cas de multi-routing
    // #[Route('/index', name: 'index_route')]
    // #[Route('/home', name: 'home_route')]
    // public function index(Request $request): Response
    // {
    //     $route = $request->attributes->get('_route');
    //     return $this->render('home/index.html.twig', ['controller_name' => $route,]);
    // }

    // L'action index sera exécutée pour les deux méthodes HTTP, GET et POST.
    // #[Route('/home', name: 'home_route', methods: ["GET", "POST"])]
    // public function index(Request $request): Response
    // {
    //     $route = $request->getMethod();
    //     return $this->render('home/index.html.twig', ['controller_name' => $route,]);
    // }

    // On va générer une route dans VehiculeController qui est redirigée vers cette action index()
    // #[Route('/home/{nom}', name: 'home_route')]
    // public function index(string $nom): Response
    // {
    //     return $this->render('home/index.html.twig', ['controller_name' => $nom,]);
    // }

    // Utilisation de HTTPException pour renvoyer vers une page d'erreur
    // #[Route('/home/{nom?}', name: 'home_route')]
    // public function index(?string $nom): Response
    // {
    //     if (!isset($nom)) {
    //         throw new HttpException(
    //             404,
    //             'On ne peut vous afficher la page de cette personne'
    //         );
    //         // // Même chose avec la méthode createNotFoundException
    //         // throw $this->createNotFoundException(
    //         //     'On ne peut vous afficher la page de cette personne'
    //         // );
    //     }
    //     return $this->render('home/index.html.twig', ['controller_name' => $nom,]);
    // }

    // Utilisation explicite de l'objet Response
    // #[Route('/home/{nom?}', name: 'home_route')]
    // public function index(?string $nom): Response
    // {
    //     $response = new Response(
    //         "<p>Bonjour $nom</p>",
    //         Response::HTTP_OK,
    //         ['content-type' => 'text/html']
    //     );
    //     return $response;
    // }

    // Récupération du chemin absolu vers notre projet
    // #[Route('/home/{nom?}', name: 'home_route')]
    // public function index(?string $nom): Response
    // {
    //     $path = $this->getParameter('kernel.project_dir');
    //     return $this->render('home/index.html.twig', ['controller_name' => $path,]);
    // }

    // Récupération d'un paramètre personnalisé définit dans config/services.yaml
    // #[Route('/home/', name: 'home_route')]
    // public function index(): Response
    // {
    //     $nom = $this->getParameter('nom');
    //     return $this->render('home/index.html.twig', ['controller_name' => $nom,]);
    // }

    // (twig p.19)
    #[Route('home', name: "home_route")]
    public function index(): Response
    {
        $personne = new Personne();
        $personne->setId(100);
        $personne->setPrenom("John");
        $personne->setNom("wick");
        $tab = [2, 3, 8];
        return $this->render('twig/index.html.twig', [
            'controller_name' => 'HomeController',
            'tableau' => $tab,
            'personne' => $personne
        ]);
    }

    // (twig p.62)
    #[Route('/menu', name: 'menu_route')]
    public function menu()
    {
        return $this->render('shared/_menu.twig', []);
    }
}
