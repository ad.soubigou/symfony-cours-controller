<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

class TwigController extends AbstractController
{
    #[Route('/twig/{nom}/{prenom}', name: 'app_twig')]
    public function index(Request $request): Response
    {
        $params = $request->get('_route_params');
        $tab = [2, 3, 8];
        return $this->render('twig/index.html.twig', [
            'controller_name' => implode(" ", $params),
            'tableau' => $tab
        ]);
    }
}
